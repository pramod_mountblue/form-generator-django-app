from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, \
    PermissionsMixin
from datetime import datetime  


class Form(models.Model):
    title = models.CharField(max_length=200)
    data = models.JSONField(null=True)
    created_at = models.DateTimeField(default=datetime.now)

    def __str__(self):
        return self.name