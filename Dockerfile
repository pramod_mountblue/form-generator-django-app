FROM python:3.8-alpine
MAINTAINER PRAMOD KUMAR

ENV PYTHONUNBUFFERED 1

# Install dependencies
COPY ./requirements.txt ./requirements.txt

RUN apk add --update --no-cache postgresql-client && \
    apk add --update --no-cache --virtual .tmp-build-deps \
    gcc libc-dev linux-headers postgresql-dev libffi-dev && \
    pip install -r /requirements.txt && \
    apk del .tmp-build-deps
 
# Setup directory structure
RUN mkdir /app
WORKDIR /app
COPY . /app

COPY ./docker-entrypoint.sh /docker-entrypoint.sh
RUN chmod +x /docker-entrypoint.sh

RUN adduser -D user && chown -R user /app
USER user

ENTRYPOINT ["/docker-entrypoint.sh"]