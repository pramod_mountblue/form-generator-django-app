from rest_framework import viewsets
from Form.serializers import FormSerializer
from Table.models import Form
from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated


class AuthViewSet(viewsets.ModelViewSet):
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    pass


class FormViewSet(AuthViewSet):
    """Manage comments in the database"""
    serializer_class = FormSerializer
    queryset = Form.objects.all()