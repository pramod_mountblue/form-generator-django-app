from django.urls import path, include
from rest_framework.routers import DefaultRouter

from Form import views

router = DefaultRouter()
router.register('all', views.FormViewSet)

app_name = 'Form'

urlpatterns = [
    path('', include(router.urls)),
]
