from rest_framework import serializers
from Table.models import Form


class FormSerializer(serializers.ModelSerializer):
    """Serializer for an ingredient object"""
    class Meta:
        model = Form
        fields = ('__all__')
        read_only_fields = ('id',)